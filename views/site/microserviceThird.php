<div class="form-group col-lg-6 offset-lg-3">
    <?= $form->field($model, 'field1',[
        'checkHorizontalTemplate' => "<div class=\"custom-control custom-checkbox\">\n{input}\n{label}\n{error}\n{hint}\n</div>"
    ])->checkbox()?>
</div>
<div class="form-group col-lg-6 offset-lg-3">
    <?= $form->field($model, 'field2') ?>
</div>
<div class="form-group array-field col-lg-6 offset-lg-3">
    <label>Field3</label>
    <?php
        foreach ($model::FIELD3_PARAMS as $key => $value) {
            echo $form->field($model, "field3[$key]", ['labelOptions' => ['label' => "$value"]]);
        }
    ?>
</div>
