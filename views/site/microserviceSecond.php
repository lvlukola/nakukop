<div class="form-group col-lg-6 offset-lg-3">
    <?= $form->field($model, 'field1') ?>
</div>
<div class="form-group col-lg-6 offset-lg-3">
    <?= $form->field($model, 'field2',[
        'checkHorizontalTemplate' => "<div class=\"custom-control custom-checkbox\">\n{input}\n{label}\n{error}\n{hint}\n</div>"
    ])->checkbox()?>
</div>
<div class="form-group col-lg-6 offset-lg-3">
    <?= $form->field($model, 'field3')?>
</div>
