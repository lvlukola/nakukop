<?php

/* @var $this yii\web\View */
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <?php
            $form = ActiveForm::begin([
                'id' => 'settings-form',
                'method' => 'post',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-3',
                        'wrapper' => 'col-sm-9',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
            ]) ?>
            <div class="form-group">
                <select id="model-index" class="form-control col-lg-6 offset-lg-3" name="modelIndex">
                    <?php
                        foreach ($models as $viewName => $moduleName) {
                            $selected = $currentModule === $viewName ? 'selected' : '';
                            echo '<option value="' . $viewName . '"' . $selected . '>' . $moduleName . '</option>';
                        }
                    ?>
                </select>
            </div>
            <?= $this->render($currentModule, ['form' => $form, 'model' => $model]) ?>
            <div class="form-group col-lg-6 offset-lg-3 buttons-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end() ?>
    </div>
</div>

<?php
$script = <<< JS
    $('#model-index').on('change', function(e) {
        const index = e.target.value;
        window.location.href = `/?module=`+index;
    });
JS;
$this->registerJs($script);
?>
