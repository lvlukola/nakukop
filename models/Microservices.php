<?php

namespace app\models;

use yii\base\Exception;
use ReflectionClass;
use app\models\MicroserviceInterface;

class Microservices
{
    private $model;
    private $models;
        
    public function __construct($models = [])
    {
        $this->model = null;
        $this->models = [];
        
        foreach ($models as $model) {
            $this->addClass($model);
        }
    }
    
    /**
     * Получение имени
     * @return string|null
     */
    private function getName($className): ?string
    {
        $path = explode('\\', $className);
        $name = array_pop($path);
        
        return lcfirst($name);
    }
    
    /**
     * Получение массива имен загруженных классов
     * @return array
     */
    public function getModels(): array
    {
        $result = [];
        
        foreach ($this->models as $viewName => $model) {
            $result[$viewName] = $model::getName();
        }
        
        return $result;
    }

    /**
     * Получение текущей модели
     * @return MicroserviceInterface|null
     */
    public function getModel(): ?MicroserviceInterface
    {
        return $this->model;
    }
    
    /**
     * Получение индекса текущей модели
     * @return int|null
     */
    /*public function getModelIndex(): ?int
    {
        return $this->modelIndex;
    }*/
    
    /**
     * Загрузка данных в модель
     * @param type $postData
     * @return bool
     */
    public function modelDataFromPost($postData): bool
    {
        $result = false;
        
        if ($this->model) {
            $result = $this->model->load($postData);
        } 

        return $result;
    }
    
    /**
     * Подгрузка текущих параметров модели
     * @return bool
     */
    public function loadModelSettings(): bool
    {
        $result = false;
        
        if ($this->model) {
            $result = $this->model->loadSettings();
        } 

        return $result;
    }
    
    /**
     * Сохранение параметров модели
     * @return bool
     */
    public function saveModelSettings(): bool
    {
        $result = false;
        
        if ($this->model) {
            $result = $this->model->saveSettings();
        } 

        return $result;        
    }
    
    /**
     * Выбор модели по имени...
     * @param string $name
     * @return bool
     * @throws Exception
     */
    public function setModel(string $name): bool
    {
        if (isset($this->models[$name])) {
            $class = $this->models[$name];
            $this->model = new $class();
            $this->modelIndex = $name;
        } else {
            throw new Exception("Class $name not set");
        }
            
        return true;
    }

    /**
     * Добавить класс модели
     * @param string $modelName
     * @return bool
     * @throws Exception
     */
    public function addClass(string $modelName): bool
    {
        $class = new ReflectionClass($modelName);
        
        if ($class->isSubclassOf(MicroserviceInterface::class)) {
            $viewName = $this->getName($modelName);
            $this->models[$viewName] = $modelName;
        } else {
            throw new Exception("$item not implemented " . MicroserviceInterface::class);
        }
        
        return true;
    }

}
