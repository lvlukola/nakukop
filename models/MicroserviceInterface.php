<?php

namespace app\models;

/**
 *
 * @author admin
 */
interface MicroserviceInterface {
    public function loadSettings(): bool;
    public function saveSettings(): bool;
    public static function getName(): string;
}
