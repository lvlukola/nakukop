<?php

namespace app\models;

use yii\base\Model;

class MicroserviceThird extends Model implements MicroserviceInterface
{
    public $field1;
    public $field2;
    public $field3;
    
    public const FIELD3_PARAMS = [
        'Параметр 1', 'Параметр 2', 'Параметр 3'
    ];


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['field1', 'boolean'],
            ['field2', 'integer'],
            ['field3', 'validateField3'],
        ];
    }
    
    public  function validateField3($attribute) 
    {
        foreach ($this->$attribute as $key => $val) {
            if (!is_int($val) && !is_string($val)) {
                $this->addError($attribute[$key], 'Field3 must contain letters or digits.');
            }
        }                
    }
    
    /**
     * Подгрузка текущих параметров
     * @return bool
     */
    public function loadSettings(): bool
    {
        $this->field1 = true;
        $this->field2 = 33;
        $this->field3 = ['string from 3 microservice', -3, 3];
        
        return true;
    }
    
    /**
     * Сохранение параметров
     * @return bool
     */
    public function saveSettings(): bool
    {
        $result = false;
        
        if ($this->validate()) {
            $result = true;            
        }
        
        return $result;
    }
    
    /**
     * Получение имени микросервиса
     * @return string
     */
    public static function getName(): string
    {
        return 'Микросервис 3';
    }
}
