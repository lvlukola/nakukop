<?php

namespace app\models;

use yii\base\Model;

class MicroserviceSecond extends Model implements MicroserviceInterface
{
    public $field1;
    public $field2;
    public $field3;
    
    public const FIELD3_PARAMS = [
        'Параметр 1', 'Параметр 2', 'Параметр 3'
    ];


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['field1', 'string'],
            ['field2', 'boolean'],
            ['field3', 'integer'],
        ];
    }
    
    /**
     * Подгрузка текущих параметров
     * @return bool
     */
    public function loadSettings(): bool
    {
        $this->field1 = 'string from 2 microservice';
        $this->field2 = false;
        $this->field3 = 222;
        
        return true;
    }
    
    /**
     * Сохранение параметров
     * @return bool
     */
    public function saveSettings(): bool
    {
        $result = false;
        
        if ($this->validate()) {
            $result = true;            
        }
        
        return $result;
    }
    
    /**
     * Получение имени микросервиса
     * @return string
     */
    public static function getName(): string
    {
        return 'Микросервис 2';
    }
}
