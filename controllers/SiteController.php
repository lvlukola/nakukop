<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Microservices;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $microServices = new Microservices([
            \app\models\MicroserviceFirst::class,
            \app\models\MicroserviceSecond::class
        ]);
        
        $microServices->addClass(\app\models\MicroserviceThird::class);

        $request = Yii::$app->request;
        
        $models = $microServices->getModels();
        $currentModule = $request->get('module') ?? array_keys($models)[0];
        
        $microServices->setModel($currentModule);

        if ($request->isPost) {            
            $postData = $request->post();
            
            if ($microServices->modelDataFromPost($postData) && $microServices->saveModelSettings()) {
                Yii::$app->session->setFlash('success', 'Настройки сохранены');
            } else {
                Yii::$app->session->setFlash('danger', 'Ошибка сохранения настроек');
            }            
        } else {
            $microServices->loadModelSettings();
        }
        
        return $this->render('index', [
            'model' => $microServices->getModel(),
            'models' => $microServices->getModels(),
            'currentModule' => $currentModule
        ]);
    }

}
